# Documentation: Creating a Crop Resource #

Editor, Contributor, or higher privileges are required to create a Crop Resource.

You may notice at some point that you are not on https://fruitandnuts.ucdavis.edu, but on https://fruitandnuts.sf.ucdavis.edu, this is ok, content will still publish to the site.

## Watch the Video Tutorial ##
![Documentation Video](https://diypbx.com/wp-content/uploads/2016/02/video-placeholder.jpg)

## Creating a Crop Resource Field by Field Documentation ##
1. [Log into the site](https://fruitsandnuts.ucdavis.edu/login)
2. Create any content that will be needed before creating a Report
    * You will need a crop to relate this to. [Check that your Crop exists](https://fruitsandnuts.sf.ucdavis.edu/admin/content?title=&type=crop&status=All&langcode=All).
    * The Resource Type will need to exist. [Check that your Resource Type exists](https://fruitsandnuts.sf.ucdavis.edu/admin/structure/taxonomy/manage/resource_type/overview).
3. Create a Crop Resource by going to [Content](https://fruitsandnuts.sf.ucdavis.edu/admin/content) > [+ Add Content](https://fruitsandnuts.sf.ucdavis.edu/node/add) > [Crop Resource](https://fruitsandnuts.sf.ucdavis.edu/node/add/crop_resource)
    1. **Resource Title** - Give your resource a title, this will be used as the resource link text.
    2. **Body** - Enter some body text. Body text is used as teaser content for featured resources.
    3. **Crop** - Check the box(s) for the Crop(s) you would like to add this resource to.
    4. **Resource Type** - Choose a resource type. If you don't see what you are looking for you can [create other resource types](https://fruitsandnuts.sf.ucdavis.edu/admin/structure/taxonomy/manage/resource_type/overview).
    5. **Link** - Add an internal or external link. If internal start typing the name of the page and choose from the suggested options. You can also use a path like <i>/node/1</i>. If external, don't forget to start with the HTTP protocol, ex. <i>http://example.com</i>.
    6. **Featured Content** - Found in the <i>Promotion Options</i> section in the sidebar, checking the box for <b>Feature Content</b> will add this resource the featured content on the Crop page(s) selected on this resource. 
    7. **Published** Check this box to publish your content upon Saving.
    8. **Save** Save your work.
4. Congratulations you have created a Crop Resource!

You can see your new resource by visiting the Crop(s) page(s) you selected when creating this resource.

    
