# Documentation: Adding a New and Exciting Article to Crop Pages #

Editor, Contributor, or higher privileges are required to create an Article and edit a Crop.

You may notice at some point that you are not on https://fruitandnuts.ucdavis.edu, but on https://fruitandnuts.sf.ucdavis.edu, this is ok, content will still publish to the site.

## Watch the Video Tutorial ##
![Documentation Video](https://diypbx.com/wp-content/uploads/2016/02/video-placeholder.jpg)

## Displaying an Article as New and Exciting on a Crop Page Step by Step ##
1. [Log into the site](https://fruitsandnuts.ucdavis.edu/login)
2. Before creating an article that will appear on a crop you must:
    * Create the Crop you would like to add the article to
    * Make sure that the crop that you would like to add it to has a <b>Primary Crop Tag</b> and make note of what that tag is. The primary crop tag can be found by editing the crop page and going to **Categorizing > Primary Crop Tags**(Found in the edit page sidebar).
3. To [Create an Article](https://sitefarm.ucdavis.edu/training/all/article-content-type) see the SiteFarm documentation.
4. Make sure that the <b>Tag</b> used in your <i>Article</i> matches the <b>Primary Crop Tag</b> on your Crop.

As long as the Article and Crop share the same tag, your Article will appear in the New and Exciting section of the Crop.
