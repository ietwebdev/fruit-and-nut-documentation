# Documentation: Creating a Report #

Editor, Contributor, or higher privileges are required to create a report.

You may notice at some point that you are not on https://fruitandnuts.ucdavis.edu, but on https://fruitandnuts.sf.ucdavis.edu, this is ok, content will still publish to the site.

## Watch the Video Tutorial ##
![Documentation Video](https://diypbx.com/wp-content/uploads/2016/02/video-placeholder.jpg)

## Creating a Report Field by Field Documentation ##
1. [Log into the site](https://fruitsandnuts.ucdavis.edu/login)
2. Create any content that will be needed before creating a Report
    * [Add a report to **Box.com**](https://ucdavis.account.box.com/login), set permissions, and get the link ready for copy/pasting
    * [Create any **collaborators**](https://fruitsandnuts.sf.ucdavis.edu/node/add/collaborator) that will be needed (Used in the Funder field)
    * Make sure that the **Publication Year** you need has been added by [checking the vocabulary](https://fruitsandnuts.sf.ucdavis.edu/admin/structure/taxonomy/manage/year/overview). (Used in the Publication Year field)
    * Make sure the [**Report Categories** you need exist](https://fruitsandnuts.sf.ucdavis.edu/admin/structure/taxonomy/manage/report/overview). (Needed to populate the Report Categories field)
3. Create a Report by going to [Content](https://fruitsandnuts.sf.ucdavis.edu/admin/content) > [+ Add Content](https://fruitsandnuts.sf.ucdavis.edu/node/add) > [Report](https://fruitsandnuts.sf.ucdavis.edu/node/add/report)
    1. **Report Title** - Give your report a name
    2. **Report Info** - This is a group of fields
        * **Box URL** - To get the [Box link](https://box.ucdavis.edu/) go to the file in Box, click share, if there is not already a share link, click the toggle to Create a shared link. Copy paste the whole link into this field. In Box, be sure that the share link permissions are set appropriately for the people who should be able to access the file. This must be an external URL such as http://example.com.
        * **Authors** - Start typing the name of the Author(s) you would like to add and choose from the suggestions. If you do not see the Author you are looking for you may type in a new Author name into this field and it will be created. You can [see and edit Authors here](https://fruitsandnuts.sf.ucdavis.edu/admin/structure/taxonomy/manage/authors/overview).
        * **Publication Year** - Choose a single year for this reports publication. Reports are sorted by year on the Funder Reports pages. If your report year does not appear in this drop down, [add it to the Year vocabulary](https://fruitsandnuts.sf.ucdavis.edu/admin/structure/taxonomy/manage/year/overview).
        * **Description** - Add a description for this report.
        * **Report Categories** - Choose any number of categories for this report. Use **CMD/CTRL + Click** to select multiple categories.
    3. **OCR Text** - Add some OCR text. OCR stands for Optical Character Recognition. It is a widespread technology to recognize text inside images, such as scanned documents and photos. In this case it will be used to make report documents such as PDFs more accessible to users with assisted technology.
    4. **Funder** - Choose the Funder for this report. If you do not see the Funder, create them by [adding a new Collaborator](https://fruitsandnuts.sf.ucdavis.edu/node/add/collaborator).
    5. **Published** Check this box to publish your content upon Saving.
    6. **Save** Save your work.
4. Congratulations you have published a Report!

You can see your reports by going to https://fruitsandnuts.sf.ucdavis.edu/reports and choosing the Funder/Collaborator. If you created a new Collaborator, you might not see it here and it will need to be added. [Learn how to add a Funder/Collaborator to the /reports page](reports-page.md).
