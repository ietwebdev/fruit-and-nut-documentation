# Documentation: Adding a Collaborator(Sometimes referred to as a funder) #

Editor, Contributor, or higher privileges are required to create a crop.

You may notice at some point that you are not on https://fruitandnuts.ucdavis.edu, but on https://fruitandnuts.sf.ucdavis.edu, this is ok, content will still publish to the site.

## Watch the Video Tutorial ##
![Documentation Video](https://diypbx.com/wp-content/uploads/2016/02/video-placeholder.jpg)

## Creating a Collaborator Step by Step ##
1. [Log into the site](https://fruitsandnuts.ucdavis.edu/login)
2. Before creating a collaborator you may want to get a jpeg or png of their logo.
3. Create a Collaborator by going to [Content](https://fruitsandnuts.sf.ucdavis.edu/admin/content) > [+ Add Content](https://fruitsandnuts.sf.ucdavis.edu/node/add) > [Collaborator](https://fruitsandnuts.sf.ucdavis.edu/node/add/collaborator)
    1. **Collaborator Name** - Enter a name for your collaborator.
    2. **Body** - Enter some body text for the collaborator page.
    3. **Collaborator Website** - Add a full URL to a collaborators website. You may add more than one by clicking the button for *Add another website*.
    4. **Collaborator Logo** - Upload a logo in one of the following file types using the Media Library; png, gif, jpg, jpeg.
        * Note: Try to use a logo with a little empty space around it or the logo will have visual issues in some of the places it displays.
    5. **Database Description** - Enter the text you want used on a crop page to link to a Funders(Collaborators) list of reports. The link will include the Funders(Collaborators) logo if uploaded.
    6. **Promotion Options > Promoted to front page** - Check this box to display the collaborator logo in the area above the footer on the home page.
    7. **Published** Check this box to publish your content upon Saving.
    8. **Save** Save your work.
    
Congratulations you have published a Collaborator! You can now:

* See the collaborator logo on the home page if promoted.
* [Add it to a Crop](crops.md) via the **Research Report Database** field on crop edit page.
* Add it to the /reports page as a link to /reports/{{ collaborator node id}}
    * See [documentation on the Reports Page](reports-page.md).
    * On /reports/{{ collaborator node id}} you will see the collaborator logo in a sidebar area labeled, "Funded by"
* Relate a report to a collaborator/funder by [Creating a report](reports.md) and choosing your collaborator from the **Funder** field.

 
    
 
        
