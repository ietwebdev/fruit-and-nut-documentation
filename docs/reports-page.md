# Documentation: Adding a Funder/Collaborator to the /reports Page #

This is a manually updated basic page. Before you do this you will need to have:

* [Created a Collaborator](https://fruitsandnuts.sf.ucdavis.edu/node/add/collaborator)
* Identified the Collaborators node id
    * To do this, [find your Collaborator](https://fruitsandnuts.sf.ucdavis.edu/admin/content?title=&type=collaborator&status=1&langcode=All)
    * Click the **Edit** button
    * Look at the URL in the browsers address bar
        * Example: https://fruitsandnuts.sf.ucdavis.edu/node/481/edit?destination=/admin/content
        * Note the part of this URL **"node/481"**
        * In this example your *Node ID* is **481**

## Add Your Collaborator Link ##

* [Go to the Reports page](https://fruitsandnuts.sf.ucdavis.edu/reports)
* Click the <b>Edit</b> button
* Go to the <b>Body</b> field and put your cursor where you want the new Funder/Collaborator
    * [Create a Teaser Link Box](https://sitefarm.ucdavis.edu/training/all/using-wysiwyg/teaser-link-box)
    * Use the following link **/reports/NODE-ID**, where **NODE-ID** is the Collaborator node id we explained how to find at the top of this article.
    * Populate an image, title, and description.
* **Save** your work.
