# Documentation: Creating a Crop #

Editor, Contributor, or higher privileges are required to create a crop.

You may notice at some point that you are not on https://fruitandnuts.ucdavis.edu, but on https://fruitandnuts.sf.ucdavis.edu, this is ok, content will still publish to the site.

## Watch the Video Tutorial ##
![Documentation Video](https://diypbx.com/wp-content/uploads/2016/02/video-placeholder.jpg)

## Creating a Crop Field by Field Documentation ##
1. [Log into the site](https://fruitsandnuts.ucdavis.edu/login)
2. Create any content that will be needed before creating a Crop
    * [Create any **collaborators**](https://fruitsandnuts.sf.ucdavis.edu/node/add/collaborator) that will be needed (Used in the Research and Report Database field)
    * Create any **production information** by [creating basic pages](https://sitefarm.ucdavis.edu/training/all/basic-page-content-type) (Used for the Production Information field)
    * Create any **weather models** by [creating basic pages](https://sitefarm.ucdavis.edu/training/all/basic-page-content-type) (Used for the Weather Model field)
    * Create any related photo galleries by [creating a photo gallery](https://fruitsandnuts.sf.ucdavis.edu/node/add/sf_photo_gallery) (Used in the Related Photo Gallery field)
3. Make sure you have graphic resources
    * Primary page image. Primary image is scaled and cropped to 2000x460px
    * Crop icon (Not sure yet on dimensions)
4. Create a Crop by going to [Content](https://fruitsandnuts.sf.ucdavis.edu/admin/content) > [+ Add Content](https://fruitsandnuts.sf.ucdavis.edu/node/add) > [Crop](https://fruitsandnuts.sf.ucdavis.edu/node/add/crop)
    1. **Title/Crop Name** - Give your crop a name
    2. **Body** - Give your crop a description ([See Using the WYSIWYG](https://sitefarm.ucdavis.edu/training/all/using-wysiwyg))
    3. **Crop Resource Grid** - This is a set of fields used to create a visual grid of info for the user
        * **Production Information** - Start typing the Title of a basic page into this auto-complete field and choose from the list
        * **Resources** - Resources are populated on this page automatically by [creating a Crop Resource](https://fruitsandnuts.sf.ucdavis.edu/node/add/crop_resource) and choosing this **Title/Crop Name** and a **Resource Type**.
        * **Weather Models** - Start typing the Title of a basic page into this auto-complete field and choose from the list. The list is curated by basic pages that have been tagged with Weather Model.
    4. **Featured Resources** - Resources are populated on this page automatically by [creating a Crop Resource](https://fruitsandnuts.sf.ucdavis.edu/node/add/crop_resource) and choosing this **Title/Crop** Name and a **Resource Type**. Resources that have the checkbox for **Featured** checked will be excluded from resources list above and instead display in this content position below the Crop Resource Grid.
    5. **Crop Images** - This is a group of image fields for the crop. Icon will display next to page title and in teaser displays accross the site, and the primary image will just show on the crop page.
        * **Primary Media** - Upload a primary image. Primary image is scaled and cropped to 2000x460px.
        * **Crop Icon** - Upload a crop icon. Crop icon will automatically be cropped and scaled to 135x135px.
        * **Note:** These images can be updated by [going to Media Content](https://fruitsandnuts.sf.ucdavis.edu/admin/content/media)
    6. **Research Report Database**(Found in the edit page sidebar) - Start typing the Title of a Collaborator into this auto-complete field and choose from the list.
    7. **New and Exciting Articles**(Found in the edit page sidebar) - Related articles are populated on this page automatically by [creating an article](https://fruitsandnuts.sf.ucdavis.edu/node/add/sf_article) and tagging it with the **Primary Crop Tag** entered for this crop.
    8. **Related Content > Related Photo Gallery**(Found in the edit page sidebar) - Choose an existing Photo Gallery to add to this crop page
    9. **Categorizing > Primary Crop Tags**(Found in the edit page sidebar) - This field is for internal purposes only. Photo galleries, articles and resources will need to share this exact tag for them to properly appear on this crop page. We recommend that this mimics the crop name, for example if this Crop is Almond, then the primary crop tag should be Almond.
    10. **Promotion Options > Promoted to frong page**(Found in the edit page sidebar) - Checking this box will feature this crop on the home page as just link text, below the crops the are promoted to front page and featured.
    11. **Promotion Options > Feature Content**(Found in the edit page sidebar) - Checking this box will feature this crop on the home page and move it to the top of the crops feature displaying this crop with an icon.
    12. **Published** Check this box to publish your content upon Saving.
    13. **Save** Save your work.
5. Congratulations you have published a Crop!

Crops will appear on the home page if featured, and will always display on the /crops page.
