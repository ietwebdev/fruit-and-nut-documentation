# Documentation: Adding a Related Photo Gallery to Crop Pages #

Editor, Contributor, or higher privileges are required to create a crop.

You may notice at some point that you are not on https://fruitandnuts.ucdavis.edu, but on https://fruitandnuts.sf.ucdavis.edu, this is ok, content will still publish to the site.

## Watch the Video Tutorial ##
![Documentation Video](https://diypbx.com/wp-content/uploads/2016/02/video-placeholder.jpg)

## Displaying a Related Photo Gallery on a Crop Page Step by Step ##
1. [Log into the site](https://fruitsandnuts.ucdavis.edu/login)
2. To [Create a Photo Gallery](https://sitefarm.ucdavis.edu/training/all/photo-gallery-content-type) see the SiteFarm documentation.
3. Now Create or Edit a Crop page and from the editing interface go to **Related Content > Related Photo Gallery**(Found in the edit page sidebar) and choose the Photo Gallery to add to this crop page by typing the name of the photo gallery and choosing it from the suggestions.
4. **Published** Check this box to publish your content upon Saving.
5. **Save** Save your work.

You should now see a teaser for your related photo gallery in the sidebar of the crop page.
