# Documentation: Managing the Home Page #

The home page has 2 primary features that you need to know a little about in order to manage them.

## ONE: The Crops ##

This is the primary feature of the home page.

* To add a crop to the area of the crops block where an icon is shown, simply create or edit a Crop and in the Promotion options check the box for **Promoted to front page** and **Feature Content**.
* To add a crop just below the crops that are featured and promoted as just a text link, only check the box for **Promoted to front page**

## TWO: The Funder/Collaborators
* To add a Collaborator logo to the bottom of the page
    * Edit a collaborator page and check the box in the Promotion Options, found in the sidebar, for **Promoted to front page**.
    
