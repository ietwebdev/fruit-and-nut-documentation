# Documentation: Fruit and Nut SiteFarm Site  #

https://fruitsandnuts.ucdavis.edu/

This documentation is for content creator and site builders.
Learn how to create the custom content types created by IET Web Dev.

Additional documentation will be provided as general knowledge.

### What is this repository for? ###

* Learn how to publish custom content types like Crops and Collaborators
* Links to relevant SiteFarm documentation
* Important links

### Custom Documentation ###

* [Manage Crops and Collaborator/Funders on the Home Page](/docs/home-page.md)
* [Create a Crop](/docs/crops.md)
* [Create a Crop Resource](/docs/cropresource.md)
* [Create a Collaborator(Funder)](/docs/collaborators.md)
* [Create a Report](/docs/reports.md)
    * [Add a Funder/Collaborator to the /reports Page](/docs/reports-page.md)
* [Create a New and Exciting Article](/docs/newandexciting.md)
* [Create a Crop Photo Gallery](/docs/cropgallery.md)

### SiteFarm Documentation ###

[SiteFarm Documentation](https://sitefarm.ucdavis.edu/training/all)

* [Create a Basic Page](https://sitefarm.ucdavis.edu/training/all/basic-page-content-type)
* [Create an Article](https://sitefarm.ucdavis.edu/training/all/article-content-type)
* [Create a Person](https://sitefarm.ucdavis.edu/training/all/person-content-type)
* [Create an Event](https://sitefarm.ucdavis.edu/training/all/event-content-type)
* [Create a Photo Gallery](https://sitefarm.ucdavis.edu/training/all/photo-gallery-content-type)

### Resources ###

* [SiteFarm Website](https://sitefarm.ucdavis.edu/)
* [SiteFarm Web Design Patterns](http://dev.webstyleguide.ucdavis.edu/redesign/?p=all)
* ...

### Who do I talk to? ###

If you have any questions or issue please email 

* [SiteFarm Service Request](https://sitefarm.ucdavis.edu/form/service-request-form)
* [Web Development Services](https://servicehub.ucdavis.edu/servicehub?id=it_catalog_content&sys_id=bebda9151bd798103f4286ae6e4bcb52)
* [Email IET Web Dev](webservices@ucdavis.edu)

### Developer Links ###

* [Custom Theme Repo](https://bitbucket.org/ietwebdev/fruit-and-nut/src/master/)
